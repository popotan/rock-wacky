package cn.rock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
// @EnableFeignClients({"com.github.wxiaoqi.security.auth.client.feign","com.github.wxiaoqi.security.gate.feign"})
@EnableZuulProxy
@EnableScheduling
// @EnableAceAuthClient
// @EnableAceGateRateLimit
public class RockGateApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockGateApplication.class, args);
	}
}
