package cn.rock.service.dto;

import java.util.List;

/**
 * @author rock
 * @date 2017/7/7
 */
public class FrontUser {
    public String id;
    public String username;
    public String name;
    private String description;
    private String image;
    private List<PermissionDto> menus;
    private List<PermissionDto> elements;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<PermissionDto> getMenus() {
        return menus;
    }

    public void setMenus(List<PermissionDto> menus) {
        this.menus = menus;
    }

    public List<PermissionDto> getElements() {
        return elements;
    }

    public void setElements(List<PermissionDto> elements) {
        this.elements = elements;
    }
}
