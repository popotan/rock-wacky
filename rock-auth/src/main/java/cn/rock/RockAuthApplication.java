package cn.rock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RockAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockAuthApplication.class, args);
	}
}
