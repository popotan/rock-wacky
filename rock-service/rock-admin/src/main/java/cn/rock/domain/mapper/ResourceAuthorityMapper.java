package cn.rock.domain.mapper;

import cn.rock.domain.model.ResourceAuthority;
import tk.mybatis.mapper.common.Mapper;

public interface ResourceAuthorityMapper extends Mapper<ResourceAuthority> {
}