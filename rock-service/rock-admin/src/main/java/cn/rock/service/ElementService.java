package cn.rock.service;

import cn.rock.domain.mapper.ElementMapper;
import cn.rock.domain.model.Element;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rock.common.base.BaseService;

import java.util.List;

/**
 * @author rock
 * @date 2017/7/7
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ElementService extends BaseService<ElementMapper,Element> {


    // @Cache(key="permission:ele:u{1}")
    public List<Element> getAuthorityElementByUserId(String userId) {
        return mapper.selectAuthorityElementByUserId ( userId );
    }

    public List<Element> getAuthorityElementByUserId(String userId, String menuId) {
        return mapper.selectAuthorityMenuElementByUserId ( userId, menuId );
    }

    // @Cache(key="permission:ele")
    public List<Element> getAllElementPermissions() {
        return mapper.selectAllElementPermissions ();
    }


    @Override
    // @CacheClear(keys={"permission:ele","permission"})
    public void insertSelective(Element entity) {
        super.insertSelective ( entity );
    }

    @Override
    // @CacheClear(keys={"permission:ele","permission"})
    public void updateSelectiveById(Element entity) {
        super.updateSelectiveById ( entity );
    }


}
