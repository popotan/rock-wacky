package cn.rock.domain.mapper;

import cn.rock.domain.model.Group;
import tk.mybatis.mapper.common.Mapper;

public interface GroupMapper extends Mapper<Group> {
}