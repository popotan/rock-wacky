package rock.common.base;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rock.common.msg.ObjectRestResponse;
import rock.common.msg.TableResultResponse;
import rock.common.utils.BaseContextHandler;
import rock.common.utils.Page;

import java.util.List;
import java.util.Map;


public class BaseController<S extends BaseService, T> {
    @Autowired
    protected S service;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ObjectRestResponse<T> add(@RequestBody T t) {
        service.insertSelective ( t );
        return new ObjectRestResponse<T> ();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<T> get(@PathVariable int id) {
        ObjectRestResponse<T> entityObjectRestResponse = new ObjectRestResponse<> ();
        Object o = service.selectById ( id );
        entityObjectRestResponse.data ( (T) o );
        return entityObjectRestResponse;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ObjectRestResponse<T> update(@RequestBody T t) {
        service.updateSelectiveById ( t );
        return new ObjectRestResponse<T> ();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ObjectRestResponse<T> remove(@PathVariable int id) {
        service.deleteById ( id );
        return new ObjectRestResponse<T> ();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<T> all() {
        return service.selectListAll ();
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    @ResponseBody
    public TableResultResponse<T> list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Page page = new Page ( params );
        return service.selectByQuery ( page );
    }

    protected String getCurrentUserName() {
        return BaseContextHandler.getUsername ();
    }
}
