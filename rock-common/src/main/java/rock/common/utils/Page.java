package rock.common.utils;


import java.util.LinkedHashMap;
import java.util.Map;


public class Page extends LinkedHashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public Page(Map<String, Object> params) {
        this.clear ();
        this.putAll ( params );
    }

    public Integer getCurrentPage() {
        return this.containsKey ( "currentPage" ) ? Integer.valueOf ( String.valueOf ( this.get ( "currentPage" ) ) )  : 1;
    }
    public Integer getPageSize() {
        return this.containsKey ( "pageSize" ) ? Integer.valueOf ( String.valueOf ( this.get ( "pageSize" ) ) ) : 10;
    }

}
