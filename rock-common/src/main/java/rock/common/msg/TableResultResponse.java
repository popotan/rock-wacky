package rock.common.msg;

import rock.common.base.BaseResponse;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-14 22:40
 */
public class TableResultResponse<T> extends BaseResponse {

    TableData<T> data;

    public TableResultResponse(long total, Integer cpage, Integer pages, List<T> rows) {
        this.data = new TableData<T> ( total, cpage, pages, rows );
    }

    public TableResultResponse() {
        this.data = new TableData<T> ();
    }

    TableResultResponse<T> total(int total) {
        this.data.setTotal ( total );
        return this;
    }

    TableResultResponse<T> total(List<T> rows) {
        this.data.setRows ( rows );
        return this;
    }

    public TableData<T> getData() {
        return data;
    }

    public void setData(TableData<T> data) {
        this.data = data;
    }

    class TableData<T> {
        long total;
        Integer currentPage;
        Integer pageSize;
        List<T> rows;

        public TableData() {
        }

        public TableData(long total, Integer currentPage, Integer pageSize, List<T> rows) {
            this.total = total;
            this.currentPage = currentPage;
            this.pageSize = pageSize;
            this.rows = rows;
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public List<T> getRows() {
            return rows;
        }

        public void setRows(List<T> rows) {
            this.rows = rows;
        }

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }
    }
}
