package cn.rock.service.api;

import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author rock
 * @date 2017/7/7
 */
@FeignClient("ace-admin")
public interface AdminServiceApi {
    // @RequestMapping(value="/api/log/save",method = RequestMethod.POST)
    // void saveLog(LogInfo info);
}
