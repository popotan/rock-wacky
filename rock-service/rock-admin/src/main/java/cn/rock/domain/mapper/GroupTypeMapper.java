package cn.rock.domain.mapper;

import cn.rock.domain.model.GroupType;
import tk.mybatis.mapper.common.Mapper;

public interface GroupTypeMapper extends Mapper<GroupType> {
}