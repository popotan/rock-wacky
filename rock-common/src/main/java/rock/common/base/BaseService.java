package rock.common.base;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import rock.common.msg.TableResultResponse;
import rock.common.utils.Page;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.entity.Example;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public abstract class BaseService<M extends Mapper<T>, T> {

    @Autowired
    protected M mapper;

    void setMapper(M mapper) {
        this.mapper = mapper;
    }

    public T selectOne(T entity) {
        return mapper.selectOne ( entity );
    }


    public T selectById(Object id) {
        return mapper.selectByPrimaryKey ( id );
    }


    public List<T> selectList(T entity) {
        return mapper.select ( entity );
    }


    public List<T> selectListAll() {
        return mapper.selectAll ();
    }


    public Long selectCount(T entity) {
        return new Long ( mapper.selectCount ( entity ) );
    }


    public void insert(T entity) {
        // EntityUtils.setCreatAndUpdatInfo ( entity );
        mapper.insert ( entity );
    }


    public void insertSelective(T entity) {
        // EntityUtils.setCreatAndUpdatInfo ( entity );
        mapper.insertSelective ( entity );
    }


    public void delete(T entity) {
        mapper.delete ( entity );
    }


    public void deleteById(Object id) {
        mapper.deleteByPrimaryKey ( id );
    }


    public void updateById(T entity) {
        // EntityUtils.setUpdatedInfo ( entity );
        mapper.updateByPrimaryKey ( entity );
    }


    public void updateSelectiveById(T entity) {
        // EntityUtils.setUpdatedInfo ( entity );
        mapper.updateByPrimaryKeySelective ( entity );

    }

    public List<T> selectByExample(Object example) {
        return mapper.selectByExample ( example );
    }

    public int selectCountByExample(Object example) {
        return mapper.selectCountByExample ( example );
    }

    public TableResultResponse<T> selectByQuery(Page page) {
        Class<T> clazz = (Class<T>) ((ParameterizedType) getClass ().getGenericSuperclass ()).getActualTypeArguments ()[1];
        Example example = new Example ( clazz );
        if (page.entrySet ().size () > 0) {
            Example.Criteria criteria = example.createCriteria ();
            for (Map.Entry<String, Object> entry : page.entrySet ()) {
                if (entry.getKey ().equals ( "currentPage" ) || entry.getKey ().equals ( "pageSize" )) {
                    continue;
                }
                criteria.andLike ( entry.getKey (), "%" + entry.getValue ().toString () + "%" );
            }
        }
        com.github.pagehelper.Page<Object> result = PageHelper.startPage ( page.getCurrentPage (), page.getPageSize () );
        List<T> list = mapper.selectByExample ( example );
        return new TableResultResponse<T> ( result.getTotal (), page.getCurrentPage (), page.getPageSize (), list );
    }
}
