package cn.rock;


import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
@MapperScan("cn.rock.domain.mapper")
public class RockAdminApplication {
    private static Logger logger = LoggerFactory.getLogger ( RockAdminApplication.class );
    public static void main(String[] args) {
        logger.debug ( "后台程序启动" );
        new SpringApplicationBuilder (RockAdminApplication.class).web(true).run(args);
    }
}
