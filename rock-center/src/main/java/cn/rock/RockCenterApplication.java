package cn.rock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class RockCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockCenterApplication.class, args);
	}
}
