package cn.rock.service;

import cn.rock.domain.mapper.MyUserMapper;
import cn.rock.domain.model.MyUser;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rock.common.base.BaseService;
import rock.common.constant.UserConstant;

/**
 * @author rock
 * @date 2017/7/7
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserService extends BaseService<MyUserMapper,MyUser> {

    @Override
    public void insertSelective(MyUser entity) {
        String password = new BCryptPasswordEncoder ( UserConstant.PW_ENCORDER_SALT).encode(entity.getPassword());
        entity.setPassword(password);
        super.insertSelective(entity);
    }

    @Override
    // @CacheClear(pre="MyUser{1.MyUsername}")
    public void updateSelectiveById(MyUser entity) {
        super.updateSelectiveById(entity);
    }

    /**
     * 根据用户名获取用户信息
     */
    // @Cache(key="MyUser{1}")
    public MyUser getMyUserByMyUsername(String MyUsername){
        MyUser MyUser = new MyUser();
        MyUser.setUsername (MyUsername);
        return mapper.selectOne(MyUser);
    }


}
