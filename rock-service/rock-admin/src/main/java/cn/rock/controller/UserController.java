package cn.rock.controller;

import cn.rock.controller.dto.MenuTree;
import cn.rock.domain.model.MyUser;
import cn.rock.service.UserService;
import cn.rock.service.biz.PermissionBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rock.common.base.BaseController;
import rock.common.pojo.FrontUser;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController extends BaseController<UserService,MyUser> {

    @Autowired
    private PermissionBiz permissionBiz;
    @RequestMapping(value = "/front/info", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUserInfo(@RequestParam("name") String token) throws Exception {
        FrontUser userInfo = permissionBiz.getUserInfo(token);
        if(userInfo==null) {
            return ResponseEntity.status(401).body(false);
        } else {
            return ResponseEntity.ok(userInfo);
        }
    }

    @RequestMapping(value = "/front/menus", method = RequestMethod.GET)
    public @ResponseBody
    List<MenuTree> getMenusByUsername(@RequestParam("name") String token) throws Exception {
        return permissionBiz.getMenusByUsername(token);
    }

}
